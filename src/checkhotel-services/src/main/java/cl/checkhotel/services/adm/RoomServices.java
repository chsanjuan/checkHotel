package cl.checkhotel.services.adm;

import java.util.Collection;

import cl.checkhotel.domain.entity.bussines.RoomEntity;

public interface RoomServices {

	void saveRoom(RoomEntity room);
	
	Collection<RoomEntity> listRoomEntities(Integer idPalace);
	
	RoomEntity roomById(Integer idRoom);
	
	void deleteRoom(Integer idRoom);
	
	void updateRoom(Integer idRoom);
	
}
