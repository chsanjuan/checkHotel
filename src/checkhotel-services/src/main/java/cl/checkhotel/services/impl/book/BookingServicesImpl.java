package cl.checkhotel.services.impl.book;

import java.util.ArrayList;
import java.util.Collection;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cl.checkhotel.dao.jpa.book.BookingRepository;
import cl.checkhotel.dao.jpa.book.BookingRoomRepository;
import cl.checkhotel.domain.entity.bussines.BookingEntity;
import cl.checkhotel.domain.entity.bussines.BookingRoomEntity;
import cl.checkhotel.services.book.BookingServices;

@Service("bookingServices")
@Transactional
public class BookingServicesImpl implements BookingServices {
	
	@Autowired
	BookingRepository bookingRepository;
	
	@Autowired
	BookingRoomRepository bookingRoomRepository;
	
	@Override
	public void cancelBooking(Integer idBooking) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void createBookings(BookingEntity booking) {
		
		try {
			
			bookingRepository.save(booking);
			
			booking.setId(bookingRepository.idLastRegister());
			
			bookingRoomRepository.save(createIterable(booking));
			
		} catch (Exception ex) {
			throw ex;
		}
		
	}


	@Override
	public void payBooking(Integer idBooking) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Collection<BookingEntity> listBooking(Integer idClient) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public BookingEntity searchIdBooking(Integer idBooking) {
		
		BookingEntity newBooking = bookingRepository.findOne(idBooking);
		
		return newBooking;
	}

	private Collection<BookingRoomEntity> createIterable(BookingEntity booking) {
		
		Collection<BookingRoomEntity> listRoomBook = new ArrayList<>();
		
		Integer id = booking.getId();
		
		for(Integer room : booking.getListRooms()){
			BookingRoomEntity bookRoom = new BookingRoomEntity();
			bookRoom.setIdBooking(id);
			bookRoom.setIdRoom(room);
			
			listRoomBook.add(bookRoom);
		}
		
		return listRoomBook;
	}

}
