package cl.checkhotel.services.book;

import java.util.Collection;

import cl.checkhotel.domain.entity.bussines.BookingEntity;

public interface BookingServices {
	
	void cancelBooking(Integer idBooking);
	
	void createBookings(BookingEntity booking);
	
	void payBooking(Integer idBooking);
	
	Collection<BookingEntity> listBooking(Integer idClient);
	
	BookingEntity searchIdBooking(Integer idBooking);

	

}
