package cl.checkhotel.services.impl.guest;

import java.util.Collection;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cl.checkhotel.dao.jpa.guest.GuestRepository;
import cl.checkhotel.domain.entity.bussines.GuestEntity;
import cl.checkhotel.services.guest.GuestService;

@Service("guestService")
@Transactional
public class GuestServicesImpl implements GuestService {
	
	@Autowired
	GuestRepository guestRepository;

	@Override
	public void saveGuest(GuestEntity guest) {
		// TODO Auto-generated method stub

	}

	@Override
	public Collection<GuestEntity> listGuestByClient(Integer idCLient) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public GuestEntity guestById(String idGuest) {
		
		return guestRepository.findOne(idGuest);
	}

	@Override
	public void deleteGuest(Integer idGuest) {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateGuest(Integer idGuest) {
		// TODO Auto-generated method stub

	}

}
