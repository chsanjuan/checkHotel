package cl.checkhotel.services.impl.book;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cl.checkhotel.dao.jpa.book.BookingRoomRepository;
import cl.checkhotel.domain.entity.bussines.BookingRoomEntity;
import cl.checkhotel.services.book.BookingRoomServices;

@Service("bookingRoomServices")
@Transactional
public class BookingRoomServicesImpl implements BookingRoomServices {
	
	@Autowired
	BookingRoomRepository bookingRoomRepository;
	
	@Override
	public BookingRoomEntity roomByBooking(Integer idBooking) {
		// TODO Auto-generated method stub
		return bookingRoomRepository.findOne(idBooking);
	}

}
