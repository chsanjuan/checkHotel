package cl.checkhotel.services.impl.adm;

import java.util.Collection;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cl.checkhotel.dao.jpa.adm.PalaceRepository;
import cl.checkhotel.domain.entity.bussines.PalaceEntity;
import cl.checkhotel.services.adm.PalaceServices;

@Service("palaceServices")
@Transactional
public class PalaceServicesImpl implements PalaceServices {
	
	@Autowired
	private PalaceRepository palaceRepository;

	@Override
	public void savePalace(PalaceEntity palace, Integer idCliente) {
		// TODO Auto-generated method stub

	}

	@Override
	public Collection<PalaceEntity> listPalaceEntities(Integer idClient) {
		
		return palaceRepository.findAllByClient(idClient);
	}

	@Override
	public PalaceEntity palaceById(Integer idPalace) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deletePalace(Integer idPalace) {
		// TODO Auto-generated method stub

	}

	@Override
	public void updatePalace(Integer idPalace) {
		// TODO Auto-generated method stub

	}

}
