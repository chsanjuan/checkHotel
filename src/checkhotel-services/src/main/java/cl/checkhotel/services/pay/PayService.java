package cl.checkhotel.services.pay;

import java.util.Collection;

import cl.checkhotel.domain.entity.bussines.PayEntity;

public interface PayService {
	
	void savePay(PayEntity pay);
	
	Collection<PayEntity> listPayEntitys(Integer idCliente);
	
	Collection<PayEntity> listPayEntitys(Integer idCliente,Integer idBooking);
	
	

}
