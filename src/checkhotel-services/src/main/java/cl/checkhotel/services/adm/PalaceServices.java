package cl.checkhotel.services.adm;

import java.util.Collection;

import cl.checkhotel.domain.entity.bussines.PalaceEntity;

public interface PalaceServices {
	
	void savePalace(PalaceEntity palace, Integer idCliente);
	
	Collection<PalaceEntity> listPalaceEntities(Integer idClient);
	
	PalaceEntity palaceById(Integer idPalace);
	
	void deletePalace(Integer idPalace);
	
	void updatePalace(Integer idPalace);
	

}
