package cl.checkhotel.services.guest;

import java.util.Collection;

import cl.checkhotel.domain.entity.bussines.GuestEntity;


public interface GuestService {

	void saveGuest(GuestEntity guest);
	
	Collection<GuestEntity> listGuestByClient(Integer idCLient);

	GuestEntity guestById(String idGuest);
	
	void deleteGuest(Integer idGuest);
	
	void updateGuest(Integer idGuest);
	
}
