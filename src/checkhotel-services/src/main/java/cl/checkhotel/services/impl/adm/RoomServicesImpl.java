package cl.checkhotel.services.impl.adm;

import java.util.Collection;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cl.checkhotel.dao.jpa.adm.RoomRepository;
import cl.checkhotel.domain.entity.bussines.RoomEntity;
import cl.checkhotel.services.adm.RoomServices;

@Service("roomServices")
@Transactional
public class RoomServicesImpl implements RoomServices{
	
	@Autowired
	private RoomRepository roomRepository;

	@Override
	public void saveRoom(RoomEntity room) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Collection<RoomEntity> listRoomEntities(Integer idPalace) {
		
		Collection<RoomEntity> list = null;
		try {
			list = roomRepository.findAllByPalace(idPalace);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return list;
	}

	@Override
	public RoomEntity roomById(Integer idRoom) {
		return roomRepository.findOne(idRoom);
	}

	@Override
	public void deleteRoom(Integer idRoom) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateRoom(Integer idRoom) {
		// TODO Auto-generated method stub
		
	}

}
