package cl.checkhotel.dao.jpa.book;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import cl.checkhotel.domain.entity.bussines.BookingEntity;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
public class BookingRepositoryTest extends AbstractJUnit4SpringContextTests {

	private static final String DATE_FROM = "2016-10-07 15:00:00.0";
	
	private static final String DATE_TO = "2016-10-10 12:00:00.0";
	
	private SimpleDateFormat FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	private static final String STATUS_BOOK = "ACP";

	private static final String ID_CLIENTE = "19";

	@Autowired
	@Qualifier("bookingRepository")
	private BookingRepository bookingRepository;

	private BookingEntity booking;

	@Before
	public void setUp() throws ParseException {
		
		booking = createBooking();

	}
	
	@After
	public void cleanUp(){
		
		bookingRepository.deleteAll();
		
		
	}

	private BookingEntity createBooking() throws ParseException {

		booking = new BookingEntity();

		booking.setDateFrom(transforStringDate(DATE_FROM));
		booking.setDateTo(transforStringDate(DATE_TO));
		booking.setGuest(ID_CLIENTE);
		booking.setStatus(STATUS_BOOK);

		return booking;
	}

	private Date transforStringDate(String strDate) throws ParseException {
		
		Date fecha = null;
		
		fecha = FORMAT.parse(strDate);

		return fecha;
	}

	@Test
	public void testSavedBooking(){
		
		bookingRepository.save(booking);
		
		BookingEntity newBooking = getBookingRecentSave();
		
		Assert.assertEquals(ID_CLIENTE, newBooking.getGuest());
		Assert.assertEquals(STATUS_BOOK, newBooking.getStatus());
		Assert.assertEquals(DATE_FROM, newBooking.getDateFrom().toString());
		Assert.assertEquals(DATE_TO, newBooking.getDateTo().toString());
		
	}

	private BookingEntity getBookingRecentSave() {

		Integer id = bookingRepository.idLastRegister();
		
		BookingEntity book = bookingRepository.findOne(id);
		
		return book;
	}
}
