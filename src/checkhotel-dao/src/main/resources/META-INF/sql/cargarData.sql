-- -----------------------------------------------------
-- Table `checkHotel`.`pais`
-- -----------------------------------------------------
INSERT INTO checkHotel.pais  (id,descripcion) values(56,'Chile');
INSERT INTO checkHotel.pais  (id,descripcion) values(54,'Argentina');
INSERT INTO checkHotel.pais  (id,descripcion) values(591,'Bolivia');
INSERT INTO checkHotel.pais  (id,descripcion) values(55,'Brasil');
INSERT INTO checkHotel.pais  (id,descripcion) values(57,'Colombia');
INSERT INTO checkHotel.pais  (id,descripcion) values(51,'Perú');
INSERT INTO checkHotel.pais  (id,descripcion) values(1,'EE.UU');
INSERT INTO checkHotel.pais  (id,descripcion) values(52,'México');
INSERT INTO checkHotel.pais  (id,descripcion) values(49,'Alemania');
INSERT INTO checkHotel.pais  (id,descripcion) values(33,'Francia');

-- -----------------------------------------------------
-- Table `checkHotel`.`region`
-- -----------------------------------------------------
INSERT INTO checkHotel.region  (id, descripcion,pais) values(15,'Arica y Parinacota',56);
INSERT INTO checkHotel.region  (id, descripcion,pais) values(1,'Tarapacá',56);
INSERT INTO checkHotel.region  (id, descripcion,pais) values(2,'Antofagasta',56);
INSERT INTO checkHotel.region  (id, descripcion,pais) values(3,'Atacama',56);
INSERT INTO checkHotel.region  (id, descripcion,pais) values(4,'Coquimbo',56);
INSERT INTO checkHotel.region  (id, descripcion,pais) values(5,'Valparaíso',56);
INSERT INTO checkHotel.region  (id, descripcion,pais) values(13,'(RM) Santiago',56);
INSERT INTO checkHotel.region  (id, descripcion,pais) values(6,'OHiggins',56);
INSERT INTO checkHotel.region  (id, descripcion,pais) values(7,'Maule',56);
INSERT INTO checkHotel.region  (id, descripcion,pais) values(8,'Bío-Bío',56);
INSERT INTO checkHotel.region  (id, descripcion,pais) values(9,'La Arauncanía',56);
INSERT INTO checkHotel.region  (id, descripcion,pais) values(14,'Los Ríos',56);
INSERT INTO checkHotel.region  (id, descripcion,pais) values(10,'Los Lagos',56);
INSERT INTO checkHotel.region  (id, descripcion,pais) values(11,'Aysén',56);
INSERT INTO checkHotel.region  (id, descripcion,pais) values(12,'Magallanes',56);

-- -----------------------------------------------------
-- Table `checkHotel`.`comuna`
-- ----------------------------------------------------
INSERT INTO checkHotel.comuna (id,descripcion,region) values(1,'Corral',14);
INSERT INTO checkHotel.comuna (id,descripcion,region) values(2,'Lanco',14);
INSERT INTO checkHotel.comuna (id,descripcion,region) values(3,'Los Lagos',14);
INSERT INTO checkHotel.comuna (id,descripcion,region) values(4,'Máfil',14);
INSERT INTO checkHotel.comuna (id,descripcion,region) values(5,'Mariquina',14);
INSERT INTO checkHotel.comuna (id,descripcion,region) values(6,'Paillaco',14);
INSERT INTO checkHotel.comuna (id,descripcion,region) values(7,'Panguipulli',14);
INSERT INTO checkHotel.comuna (id,descripcion,region) values(8,'Valdivia',14);
INSERT INTO checkHotel.comuna (id,descripcion,region) values(9,'Futrono',14);
INSERT INTO checkHotel.comuna (id,descripcion,region) values(10,'La Unión',14);
INSERT INTO checkHotel.comuna (id,descripcion,region) values(11,'Lago Ranco',14);
INSERT INTO checkHotel.comuna (id,descripcion,region) values(12,'Río Bueno',14);

-- -----------------------------------------------------
-- Table `checkHotel`.`tipoHospedaje`
-- -----------------------------------------------------
INSERT INTO checkHotel.tipoHospedaje  (id,descripcion,indicadorTipo) values(1,'Habitación Simple',0);
INSERT INTO checkHotel.tipoHospedaje  (id,descripcion,indicadorTipo) values(2,'Habitación doble',0);
INSERT INTO checkHotel.tipoHospedaje  (id,descripcion,indicadorTipo) values(3,'Habitación Triple',0);
INSERT INTO checkHotel.tipoHospedaje  (id,descripcion,indicadorTipo) values(4,'Suite',0);
INSERT INTO checkHotel.tipoHospedaje  (id,descripcion,indicadorTipo) values(5,'Cabaña',0);
INSERT INTO checkHotel.tipoHospedaje  (id,descripcion,indicadorTipo) values(6,'Casa',0);
INSERT INTO checkHotel.tipoHospedaje  (id,descripcion,indicadorTipo) values(7,'ApartHotel',1);
INSERT INTO checkHotel.tipoHospedaje  (id,descripcion,indicadorTipo) values(8,'Hotel',1);
INSERT INTO checkHotel.tipoHospedaje  (id,descripcion,indicadorTipo) values(9,'Motel',1);
INSERT INTO checkHotel.tipoHospedaje  (id,descripcion,indicadorTipo) values(10,'Centro Alojamiento',1);
INSERT INTO checkHotel.tipoHospedaje  (id,descripcion,indicadorTipo) values(11,'Resort',1);
INSERT INTO checkHotel.tipoHospedaje  (id,descripcion,indicadorTipo) values(12,'Corretajes',1);

-- -----------------------------------------------------
-- Table `checkHotel`.`cliente`
-- -----------------------------------------------------
INSERT INTO checkHotel.cliente (rutRepresentante,direccion,email,descripcion,nombreRep,apellidoRep,sitioWeb) values('103459877','Guido beck de ramberga 246','emial','Corretajes Coñaripe','Cecilia','Villanueva','www.corretaje.cl');
INSERT INTO checkHotel.cliente (rutRepresentante,direccion,email,descripcion,nombreRep,apellidoRep,sitioWeb) values('19','Miami 3325','emial','Hotel','Ronald','Fernandez','www.Hotel.cl');

-- -----------------------------------------------------
-- Table `checkHotel`.`usuario`
-- -----------------------------------------------------
INSERT INTO checkHotel.usuario (id,password,cliente,nombre,apellido,email,onLine,passAttempt,estado) values('103459877','01234',1,'Cecilia','Villanueva','email',0,3,0);

-- -----------------------------------------------------
-- Table `checkHotel`.`pasajero`
-- -----------------------------------------------------
INSERT INTO checkHotel.pasajero (id,nombre,apellido,email,pais,estado) values('19','Ronald','Fernandez','email',56,0);

-- -----------------------------------------------------
-- Table `checkHotel`.`centros`
-- -----------------------------------------------------
INSERT INTO checkHotel.centros (direccion,descripcion,telefono,estrellas,comuna,responsable,tipo,cliente,caracteristicas,estado,comentario) values('Guido beck de ramberga 246','Corretaje Coñaripe', 974768771,0,7,'Cecilia Villanueva',5,1,'Estacionamiento',0,'Corretajes coñaripe');
INSERT INTO checkHotel.centros (direccion,descripcion,telefono,estrellas,comuna,responsable,tipo,cliente,caracteristicas,estado,comentario) values('Guido beck de ramberga 246','prueba 1', 974768771,0,7,'Robert Landdog',5,1,'Estacionamiento',0,'prueba 1');
INSERT INTO checkHotel.centros (direccion,descripcion,telefono,estrellas,comuna,responsable,tipo,cliente,caracteristicas,estado,comentario) values('Miami 3325','Hotel', 974768772,0,7,'Ronald Fernández',5,2,'Estacionamiento',0,'mi hotel');


-- -----------------------------------------------------
-- Table `checkHotel`.`habitacion`
-- -----------------------------------------------------
INSERT INTO checkHotel.habitacion (centro,descripcion,tipo,valor,numeroPersonas,linkGaleria,caracteristicas,comentario) values(1,'Habitación 1',12,100000,8,'facebook.com','wifi,2 baños,terraza,piscina,agua caliente','Cabaña super super linda');
INSERT INTO checkHotel.habitacion (centro,descripcion,tipo,valor,numeroPersonas,linkGaleria,caracteristicas,comentario) values(1,'Habitación 2',12,150000,8,'facebook.com','wifi,2 baños,terraza,piscina,agua caliente','Cabaña super super linda');
INSERT INTO checkHotel.habitacion (centro,descripcion,tipo,valor,numeroPersonas,linkGaleria,caracteristicas,comentario) values(2,'prueba 1',12,100000,8,'facebook.com','wifi','Cabaña');
