package cl.checkhotel.dao.jpa.book;

import org.springframework.data.jpa.repository.JpaRepository;

import cl.checkhotel.domain.entity.bussines.PayEntity;

public interface PayRepository extends JpaRepository<PayEntity, Integer> {

}
