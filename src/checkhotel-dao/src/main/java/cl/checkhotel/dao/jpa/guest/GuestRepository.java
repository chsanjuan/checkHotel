package cl.checkhotel.dao.jpa.guest;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import cl.checkhotel.domain.entity.bussines.GuestEntity;

@Repository
public interface GuestRepository extends JpaRepository<GuestEntity, String> {

}
