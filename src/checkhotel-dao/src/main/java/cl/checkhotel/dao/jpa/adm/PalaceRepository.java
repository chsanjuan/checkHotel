package cl.checkhotel.dao.jpa.adm;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import cl.checkhotel.domain.entity.bussines.PalaceEntity;

@Repository
public interface PalaceRepository extends JpaRepository<PalaceEntity, Integer> {
	
	@Query("select p from PalaceEntity p where p.client = :clientId")
	Collection<PalaceEntity> findAllByClient(@Param("clientId") Integer clientId);

}
