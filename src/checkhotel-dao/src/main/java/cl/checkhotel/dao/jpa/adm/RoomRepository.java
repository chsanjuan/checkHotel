package cl.checkhotel.dao.jpa.adm;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import cl.checkhotel.domain.entity.bussines.RoomEntity;

@Repository
public interface RoomRepository extends JpaRepository<RoomEntity, Integer> {
	
	@Query("select e from RoomEntity e where e.palace = :palaceId")
	Collection<RoomEntity> findAllByPalace(@Param("palaceId") Integer palaceId);
	

}
