package cl.checkhotel.dao.jpa.book;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import cl.checkhotel.domain.entity.bussines.BookingEntity;

@Repository
public interface BookingRepository extends JpaRepository<BookingEntity, Integer> {
	
	@Query("select MAX(id) from BookingEntity")
	Integer idLastRegister();
}
