package cl.checkhotel.web.frontend;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.springframework.web.servlet.ModelAndView;

import cl.checkhotel.web.frontend.Controller.BaseController;

public class BaseControllerTest {
	
	private static final String VIEW_MODEL = "index";
	
	private BaseController controller;
	
	@Before 
	public void setUp(){
		controller = new BaseController();
		
	}

	@Test
    public void testHandleRequestView() throws Exception{		
        ModelAndView modelAndView = controller.handleRequest(null, null);
        
        assertEquals(VIEW_MODEL, modelAndView.getViewName());
    }

}
