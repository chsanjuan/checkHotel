<%@page import="org.apache.velocity.runtime.directive.Include"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false" %>

<%@include file="/resources/jsp/include.jsp" %>
<html>
 
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<%--     <title><spring:message code="title"/></title> --%>
    
    <title>Corretaje Co�aripe</title>
    
	<link href="<c:url value='/resources/css/conaripe.css'  />" rel="stylesheet" />
	<link href="<c:url value='/resources/css/prettyPhoto.css'/>" rel="stylesheet" />
	<link href="<c:url value='/resources/css/ui.datepicker.css'/>" rel="stylesheet" />
	
	<script src="<c:url value='/resources/js/jquery-1.9.1.min.js' />"></script>
	<script src="<c:url value='/resources/js/jquery-ui.custom.js' />"></script>
	<script src="<c:url value='/resources/js/datepicker-es.js' />"></script>
	<script src="<c:url value='/resources/js/util.js' />"></script>
	
	<!--[if IE]>
    	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    
	
<script type="text/javascript">
jQuery(function($){
	$.datepicker.regional['es'] = {
		closeText: 'Cerrar',
		prevText: '&#x3c;Ant',
		nextText: 'Sig&#x3e;',
		currentText: 'Hoy',
		monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio',
		'Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
		monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun',
		'Jul','Ago','Sep','Oct','Nov','Dic'],
		dayNames: ['Domingo','Lunes','Martes','Mi&eacute;rcoles','Jueves','Viernes','S&aacute;bado'],
		dayNamesShort: ['Dom','Lun','Mar','Mi&eacute;','Juv','Vie','S&aacute;b'],
		dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','S&aacute;'],
		weekHeader: 'Sm',
		dateFormat: 'dd/mm/yy',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''};
	$.datepicker.setDefaults($.datepicker.regional['es']);
});    
 </script>
	
</head>
  
<body>
        <header id="header">
            <tiles:insertAttribute name="header" />
        </header>
     
<!--         <section id="sidemenu"> -->
<%--             <tiles:insertAttribute name="menu" /> --%>
<!--         </section> -->
        
        <section id="site-content">
            <tiles:insertAttribute name="content" />
        </section>
         
        <footer id="footer">
            <tiles:insertAttribute name="footer" />
        </footer>
</body>
