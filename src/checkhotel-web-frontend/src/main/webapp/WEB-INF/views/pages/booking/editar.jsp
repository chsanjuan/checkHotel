<%@include file="/resources/jsp/include.jsp" %>
<section id="mainRight">
	<div class="mainEspacio" ></div> 
	<blockquote>Aquí puedes revisar tu reserva, editarla o eliminarla.  La acción de eliminación no puede deshacerse.</blockquote>

	<form:form action="booking/searchBooking" method="POST" commandName="bookingEntity">
		<ul class="disponibilidad_lista">
			<li><img src="${pageContext.request.contextPath}/img/cabanas/cabana1.jpg" alt="">
				<h2>${bookingEntity.roomEntity.description} CABA&Ntilde;A ARAUCARIA (10 personas) $120.000 / US $170</h2>
				<div>
					<strong>DETALLES DEL TITULAR</strong>
					<ul>
						<li>
							<label>Nombre:</label>
							<form:input path="bookingEntity.guestEntity.fullName" type="text" size="12" value="bookingEntity.guestEntity.fullName" />
						</li>
					</ul>
					<p>
					&bull; Juan Alberto Pérez González<br>
					&bull; Cédula de Identidad: 12.345.678-9<br>
					&bull; Tarjeta de Crédito: Visa<br>
					&bull; Número de Tarjeta: 1234 5678 9012 345<br>
					</p>
					<strong>DETALLES DE LA RESERVA</strong>
					<p>
					&bull; Cabaña:  Araucaria<br>
					&bull; Check In:  23 de enero de 2017<br>
					&bull; Check Out:  30 de enero de 2017<br>
					&bull; Huéspedes Adultos: 6<br>
					&bull; Huéspedes Niños: 4<br>
					</p>
					<strong>VALOR TOTAL: $840.000 CLP / $1200 USD</strong>
					<h3 style="background-color:#fff; padding:10px; box-sizing:border-box; margin:20px 0px 0px -10px ">ID DE RESERVA : 12345DDEEEE</h3>
					<a class="button" href="index.html">Cerrar</a >
					<a class="button" href="registro.html">Modificar</a >
					<a class="button" href="mireserva.html">Eliminar Reserva</a >
				</div>
			</li>
		</ul>       
	</form:form>   
		   
</section>