<%@include file="/resources/jsp/include.jsp" %>
<div class="reserva_cont">
		<h1><a href="${pageContext.request.contextPath}/">Corretajes Co�aripe</a></h1>
		<h3>Mi Reserva</h3>
		<div>En esta secci&oacute;n puedes realizar una nueva reserva, revisar los datos de la reserva que ya has hecho o modificarla, todo de manera r&aacute;pida y 100% online.</div>
		</div> 
		
<section id="mainRight">
	<div><img src="${pageContext.request.contextPath}/resources/img/foto03.jpg" alt="" title="" border="0" /></div>
	<div class="mainEspacio" ></div> 
	<blockquote>Co&ntilde;aripe (del mapudungun Konha r&uuml;p&uuml;, 'sendero del guerrero') es un balneario ubicado en la ribera oriente del lago Calafqu&eacute;n, se ha convertido en un activo balneario tur&iacute;stico, gracias a su extensa playa de 3 km de largo y cercan&iacute;a de numerosas termas y del volc&aacute;n Villarrica, dando la posibilidad de realizar actividades no solamente en verano, sino tambi&eacute;n durante todo el a&ntilde;o.</blockquote>
		
	<h2>Haz Tu Reserva</h2>

	<p><strong>Todas nuestras caba�as cuentan con:</strong><br>
	Vajilla - Microondas - Refrigerador - Agua caliente - Comedor - Sala de Estar - Televisi�n satelital - Calefacci�n a combusti�n lenta - Parrillas para asados</p>

	<form>
		<ul class="reserva">
			<li>
				<label>Check in:</label>
				<input type="text" name="datepicker" id="datepicker" readonly="readonly" size="12" />
			</li>
			<li>
				<label>Check out:</label>
				<input type="text" name="datepicker" id="datepicker2" readonly="readonly" size="12" />
			</li>
			<li>
				<label>Hu�spedes:</label>  
				<select id="selectmenu1">
					<option selected="selected">1</option>
					<option>2</option>
					<option>3</option>
					<option>4</option>
				</select>
			</li>
			<li>
				<label>Residencia:</label>  
				<select id="selectmenu2">
					<option selected="selected">Habitaci�n</option>
					<option>Caba�a</option>
				</select>
				<a href="disponibilidad.html" class="button">Ver Disponibilidad</a >
			</li>
		</ul>
	</form>

	<h2>Revisa o Modifica tu Reserva</h2>
	<form:form action="booking/searchBooking" method="POST" modelAttribute="bookingEntity">
		<ul class="reserva">
			<li style="width:80%">
				<label>C�digo de Reserva <span style="font-size:0.7em;">(enviado a tu correo electr�nico):</span></label>
				
				<form:input path="id" type="number" size="12" />
			</li>
	  		<li>
	  			<button type="submit">Revisar Reserva</button>
	  		</li>
		</ul>
	</form:form>

</section>
<script type="text/javascript">
	$(document).ready(function() {
	   $("#datepicker").datepicker();
	   $("#datepicker2").datepicker();
	 });
 </script>