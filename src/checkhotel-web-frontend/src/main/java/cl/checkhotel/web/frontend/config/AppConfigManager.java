package cl.checkhotel.web.frontend.config;

public class AppConfigManager {

	private Integer codClient;
	
	private String emailClient;

	public Integer getCodClient() {
		return codClient;
	}

	public void setCodClient(Integer codClient) {
		this.codClient = codClient;
	}

	public String getEmailClient() {
		return emailClient;
	}

	public void setEmailClient(String emailClient) {
		this.emailClient = emailClient;
	}
	
	
}
