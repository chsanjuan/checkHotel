package cl.checkhotel.web.frontend.Controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import cl.checkhotel.domain.entity.bussines.BookingEntity;
import cl.checkhotel.domain.entity.bussines.BookingRoomEntity;
import cl.checkhotel.services.adm.RoomServices;
import cl.checkhotel.services.book.BookingRoomServices;
import cl.checkhotel.services.book.BookingServices;
import cl.checkhotel.services.guest.GuestService;

@Controller
@RequestMapping("/booking")
public class BookingController {

	private final static org.slf4j.Logger logger = LoggerFactory
			.getLogger(BookingController.class);

	@Autowired
	private BookingServices bookingServices;

	@Autowired
	private GuestService guestServices;

	@Autowired
	private RoomServices roomServices;

	@Autowired
	private BookingRoomServices bookingRoomServices;

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView welcome(Model model) {

		String now = (new Date()).toString();
		logger.info("BookingController " + now);

		model.addAttribute("bookingEntity", new BookingEntity());

		return new ModelAndView("booking/miReserva");
	}

//	@RequestMapping(value = "/searchBooking", method = RequestMethod.POST)
//	public ModelAndView searchBooking(
//			@ModelAttribute BookingEntity bookingEntity, Model model) {

//		bookingEntity = bookingServices.searchIdBooking(bookingEntity.getId());
//
//		bookingEntity.setGuestEntity(guestServices.guestById(bookingEntity
//				.getGuest()));
//
//		bookingEntity.setRoomEntity(roomServices
//				.roomById(getIdRoom(bookingEntity.getId())));
//		
//model.addAttribute("bookingEntity", bookingEntity);
//		return new ModelAndView("/editarBooking");
//
//	}
	@RequestMapping(value = "/searchBooking", method = RequestMethod.POST)
	public ModelAndView list(HttpServletRequest request,
		HttpServletResponse response, BookingEntity bookingEntity) throws Exception {

		return new ModelAndView("/editarBooking");

	}

	private Integer getIdRoom(Integer idBook){
		
		BookingRoomEntity bookBooking = bookingRoomServices.roomByBooking(idBook);
		
		return bookBooking.getIdRoom();
	}
}
