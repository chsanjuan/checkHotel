package cl.checkhotel.web.frontend.Controller;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cl.checkhotel.domain.entity.bussines.PalaceEntity;
import cl.checkhotel.domain.entity.bussines.RoomEntity;
import cl.checkhotel.services.adm.PalaceServices;
import cl.checkhotel.services.adm.RoomServices;
import cl.checkhotel.web.frontend.config.AppConfigManager;

@Controller
@RequestMapping("/rooms")
public class RoomController {

	private final static org.slf4j.Logger logger = LoggerFactory
			.getLogger(RoomController.class);

	@Autowired
	private AppConfigManager appConfigManager;

	@Autowired
	private RoomServices roomServices;

	@Autowired
	private PalaceServices palaceServices;

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView welcome(Model model) {

		String now = (new Date()).toString();
		logger.info("AboutUsController " + now);

		model.addAttribute("palace", new PalaceEntity());

		return new ModelAndView("rooms/cabanas");
	}

	@RequestMapping(value = "/getRoomForPalace", method = RequestMethod.POST)
	@ResponseBody
	public Map<Integer, RoomEntity> listAllRoom(@RequestParam Integer idPalace) {
		
		Map<Integer, RoomEntity> mapRoom = new LinkedHashMap<Integer, RoomEntity>();

		for (RoomEntity room : roomServices.listRoomEntities(idPalace)) {
			mapRoom.put(room.getId(), room);
		}

		return mapRoom;

	}

	@ModelAttribute("palaceList")
	public Map<Integer, String> listAllPalace() {

		Map<Integer, String> mapPalace = new LinkedHashMap<Integer, String>();

		for (PalaceEntity palace : palaceServices
				.listPalaceEntities(appConfigManager.getCodClient())) {
			mapPalace.put(palace.getId(), palace.getDescription());
		}

		return mapPalace;

	}

}
