package cl.checkhotel.domain.entity.bussines;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang3.StringUtils;

@Entity
@Table(name = "pasajero")
public class GuestEntity {

	@Id
	@Column(name = "id")
	private String id;

	@Column(name = "nombre")
	private String firstName;
	
	@Column(name = "apellido")
	private String lastName;
	
	private String email;
	
	@Column(name = "pais")
	private int country;

	
	/**
	 * obtiene el identificador del pasajero
	 */
	public String getId() {
		return id;
	}

	/**
	 * setea el identificador del pasajero
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * obtiene el nombre del pasajero
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * setea el nombre del pasajero
	 */
	public void setFirstName(String name) {
		this.firstName = name;
	}

	/**
	 * obtiene el apellido del pasajero
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * setea el apellido del pasajero
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * obtiene el correo electronico del pasajero
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * setea el correo electronico del pasajero
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * obtiene el pais del pasajero
	 */
	public int getCountry() {
		return country;
	}

	/**
	 * setea el pais del pasajero
	 */
	public void setCountry(int country) {
		this.country = country;
	}
	
	public String getFullName(){
		return StringUtils.join(new String[] { firstName, lastName }, ' ');
	}

}
