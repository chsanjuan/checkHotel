package cl.checkhotel.domain.entity.bussines;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import cl.checkhotel.domain.entity.based.BasedEntity;

@Entity
@Table(name = "habitacion")
public class RoomEntity extends BasedEntity {
	

	@Column(name = "centro")
	private Integer palace;
	
	@Column(name = "tipo")
	private int typeRoom;
	
	@Column(name = "valor")
	private BigDecimal price;
	
	@Column(name = "numeroPersonas")
	private Integer numberPerson;
	
	@Column(name = "linkGaleria")
	private String link;
	
	@Column(name = "caracteristicas")
    private String propertys;
	
	@Column(name = "comentario")
    private String comment;
	
	
	/**
	 * @return the palace
	 */
	public int getPalace() {
		return palace;
	}

	/**
	 * @param palace the palace to set
	 */
	public void setPalace(int palace) {
		this.palace = palace;
	}

	/**
	 * @return the typeRoom
	 */
	public int getTypeRoom() {
		return typeRoom;
	}

	/**
	 * @param typeRoom the typeRoom to set
	 */
	public void setTypeRoom(int typeRoom) {
		this.typeRoom = typeRoom;
	}

	/**
	 * @return the price
	 */
	public BigDecimal getPrice() {
		return price;
	}

	/**
	 * @param price the price to set
	 */
	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	/**
	 * @return the numberPerson
	 */
	public int getNumberPerson() {
		return numberPerson;
	}

	/**
	 * @param numberPerson the numberPerson to set
	 */
	public void setNumberPerson(int numberPerson) {
		this.numberPerson = numberPerson;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getPropertys() {
		return propertys;
	}

	public void setPropertys(String propertys) {
		this.propertys = propertys;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public void setPalace(Integer palace) {
		this.palace = palace;
	}

	public void setNumberPerson(Integer numberPerson) {
		this.numberPerson = numberPerson;
	}

	
	
	
}
 