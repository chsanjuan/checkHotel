package cl.checkhotel.domain.entity.based;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class BasedId {
	
	@Id
	@GeneratedValue
	private Integer id;

	/**
	 * Obtiene el id del objeto
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * setea el id de los objetos
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	
	
}
