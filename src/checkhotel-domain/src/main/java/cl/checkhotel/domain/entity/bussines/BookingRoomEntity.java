package cl.checkhotel.domain.entity.bussines;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "reservaHabitacion")
public class BookingRoomEntity implements Serializable  {
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 8246766081367156954L;

	@Id
	@Column(name = "idReserva")
	private Integer idBooking;
	
	@Column(name = "idHabitacion")
	private Integer idRoom;

	public Integer getIdBooking() {
		return idBooking;
	}

	public void setIdBooking(Integer idBooking) {
		this.idBooking = idBooking;
	}

	public Integer getIdRoom() {
		return idRoom;
	}

	public void setIdRoom(Integer idRoom) {
		this.idRoom = idRoom;
	}
	
	
}
