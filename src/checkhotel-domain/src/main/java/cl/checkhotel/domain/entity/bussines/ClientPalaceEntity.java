package cl.checkhotel.domain.entity.bussines;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "clienteCentros")
public class ClientPalaceEntity implements Serializable  {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5086681242306609376L;

	@Id
	@Column(name = "idCliente")
	private Integer idClient;
	
	@Column(name = "idCentros")
	private Integer idPalace;

	public Integer getIdClient() {
		return idClient;
	}

	public void setIdClient(Integer idClient) {
		this.idClient = idClient;
	}

	public Integer getIdPalace() {
		return idPalace;
	}

	public void setIdPalace(Integer idPalace) {
		this.idPalace = idPalace;
	}
	
	

}
