package cl.checkhotel.domain.model;

import org.apache.commons.lang3.StringUtils;

public class SendEmailModel {
	
	private String name;
	
	private String comment;
	
	private String emailTo;
	
	private String subject;
	
	private String emailFrom;
	

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getEmailTo() {
		return emailTo;
	}

	public void setEmailTo(String emailTo) {
		this.emailTo = emailTo;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getEmailFrom() {
		return emailFrom;
	}

	public void setEmailFrom(String emailFrom) {
		this.emailFrom = emailFrom;
	}
	
	public String getMessage(){
		
		String header = "Estimados, Mi nombre es " + this.name + ", mi correo es: "+ this.emailFrom +" ,mi consulta es la siguiente:" + "\r\n";
		
		return StringUtils.join(new String[] { header, this.comment}, ' ');
	}
	
	public String getMessage(Integer number){
		
		String header = "Querido, " + this.name + ", su número de reserva es: "+ number + ".\r\n";
		
		return StringUtils.join(new String[] { header, this.comment}, ' ');
		
	}
	

}
