package cl.checkhotel.domain.entity.bussines;

import java.util.Collection;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import cl.checkhotel.domain.entity.based.BasedId;

@Entity
@Table(name = "reserva")
public class BookingEntity extends BasedId {

	@Column(name = "pasajero")
	private String guest;

	@Column(name = "estado")
	private String status;

	@Column(name = "fechaInicio")
	private Date dateTo;

	@Column(name = "fechaTermino")
	private Date dateFrom;
	
	@Transient
	private Collection<Integer> listRooms;
	
	@Transient
	private GuestEntity guestEntity;
	
	@Transient
	private RoomEntity roomEntity;

	/**
	 * Obtiene el pasajero dueño de la reserva
	 */
	public String getGuest() {
		return guest;
	}

	/**
	 * Setea el pasajero que registro la reserva
	 */
	public void setGuest(String guest) {
		this.guest = guest;
	}

	/**
	 * Obtiene el estado actual de la reserva
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the dateTo
	 */
	public Date getDateTo() {
		return dateTo;
	}

	/**
	 * @param dateTo
	 *            the dateTo to set
	 */
	public void setDateTo(Date dateTo) {
		this.dateTo = dateTo;
	}

	/**
	 * @return the dateFrom
	 */
	public Date getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(Date dateFrom) {
		this.dateFrom = dateFrom;
	}

	public Collection<Integer> getListRooms() {
		return listRooms;
	}

	public void setListRooms(Collection<Integer> listRooms) {
		this.listRooms = listRooms;
	}

	public GuestEntity getGuestEntity() {
		return guestEntity;
	}

	public void setGuestEntity(GuestEntity guestEntity) {
		this.guestEntity = guestEntity;
	}

	public RoomEntity getRoomEntity() {
		return roomEntity;
	}

	public void setRoomEntity(RoomEntity roomEntity) {
		this.roomEntity = roomEntity;
	}

}
