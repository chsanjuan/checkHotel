package cl.checkhotel.domain.basicEnum;

public enum PaymentType {
	
	CASH (0,"En Efectivo"),
    DEBIT(1,"Tarjeta de Dédito"),
    CREDIT (2,"Tarjeta de Crédito"),
    TRANSFER(4,"Transferencia Bancaria");
	
	private Integer id;
	
	private String desc;

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @return the desc
	 */
	public String getDesc() {
		return desc;
	}

	private PaymentType(Integer id, String desc) {
		this.id = id;
		this.desc = desc;
	}

	
	

}
