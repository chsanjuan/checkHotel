package cl.checkhotel.domain.base;

import java.util.Date;

/**
 * Representa un rango de fechas.
 */
public class DateRange {

	private Date from;
	private Date to;

	/**
	 * Obtiene la fecha de inicio del rango.
	 */
	public Date getFrom() {
		return from;
	}

	/**
	 * Establece la fecha de inicio del rango.
	 */
	public void setFrom(Date from) {
		this.from = from;
	}

	/**
	 * Obtiene la fecha de término del rango.
	 */
	public Date getTo() {
		return to;
	}

	/**
	 * Establece la fecha de término del rango.
	 */
	public void setTo(Date to) {
		this.to = to;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((to == null) ? 0 : to.hashCode());
		result = prime * result + ((from == null) ? 0 : from.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DateRange other = (DateRange) obj;
		if (to == null) {
			if (other.to != null)
				return false;
		} else if (!to.equals(other.to))
			return false;
		if (from == null) {
			if (other.from != null)
				return false;
		} else if (!from.equals(other.from))
			return false;
		return true;
	}

	/**
	 * Inicializa la instancia.
	 */
	public DateRange() {

	}

	/**
	 * Inicializa la instancia con los valores especificados.
	 * 
	 * @param from
	 *            El inicio del rango. Puede ser null si es intervalo abierto.
	 * @param to
	 *            El término del rango. Puede ser null si es intervalo abierto.
	 */
	public DateRange(Date from, Date to) {
		this.from = from;
		this.to = to;
	}

}