package cl.checkhotel.domain.entity.bussines;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import cl.checkhotel.domain.entity.based.BasedEntity;

@Entity
@Table(name = "centros")
public class PalaceEntity extends BasedEntity{

	@Column(name = "cliente")
	private Integer client;	
	
	@Column(name = "comuna")
	private Integer district;
	
	@Column(name = "direccion")
	private String address;	
	
	@Column(name = "telefono")
	private Integer phone;	
	
	@Column(name = "estrellas")
	private Integer start;	
	
	@Column(name = "responsable")
	private String manager;	
	
	@Column(name = "tipo")
	private Integer typePalace;
	
	@Column(name = "caracteristicas")
	private String propertys;
	
	@Column(name = "estado")
	private Integer status;
	
	@Column(name = "comentario")
    private String comment;

	public Integer getClient() {
		return client;
	}

	public void setClient(Integer client) {
		this.client = client;
	}

	public int getDistrict() {
		return district;
	}

	public void setDistrict(int district) {
		this.district = district;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Integer getPhone() {
		return phone;
	}

	public void setPhone(Integer phone) {
		this.phone = phone;
	}

	public int getStart() {
		return start;
	}

	public void setStart(int start) {
		this.start = start;
	}

	public String getManager() {
		return manager;
	}

	public void setManager(String manager) {
		this.manager = manager;
	}

	public int getTypePalace() {
		return typePalace;
	}

	public void setTypePalace(int typePalace) {
		this.typePalace = typePalace;
	}

	public String getPropertys() {
		return propertys;
	}

	public void setPropertys(String propertys) {
		this.propertys = propertys;
	}

	public void setDistrict(Integer district) {
		this.district = district;
	}

	public void setStart(Integer start) {
		this.start = start;
	}

	public void setTypePalace(Integer typePalace) {
		this.typePalace = typePalace;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}	
	
	
}
