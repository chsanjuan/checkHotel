package cl.checkhotel.domain.entity.based;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

@MappedSuperclass
public class BasedEntity extends BasedId {
	
	@NotNull
	@NotEmpty
	@Size(max = 80)
	@Column(name="descripcion")
	private String description;

	/**
	 * Obtiene la descripción o nombre de los objetos
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Setea la descripción o nombres de las objetos
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	
}
